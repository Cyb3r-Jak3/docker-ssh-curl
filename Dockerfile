ARG ALPINE_VERSION=14
FROM alpine:3.${ALPINE_VERSION}
RUN apk update \
  && apk add --no-cache openssh-client ca-certificates bash curl \
  && mkdir -p ~/.ssh \
  && touch ~/.ssh/known_hosts \
  && chmod 700 ~/.ssh \
  && chmod 644 ~/.ssh/known_hosts \
  && eval $(ssh-agent -s)